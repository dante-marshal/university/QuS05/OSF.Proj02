#!/usr/bin/python

import sys
import string, random
random.seed()


def createMatrixInFile(fout, m, n):
    fout.write(f"{str(m)}\t{str(n)}\n")

    for i in range(m):
        newline = ''
        for j in range(n):
            newline += f"{random.uniform(-114,114):.2f}\t"
        fout.writelines(newline.strip() + '\n')


def main(m, n, p):
    fout = open('input', 'w')
    createMatrixInFile(fout, m, n)
    fout.write('\n')
    createMatrixInFile(fout, n, p)
    fout.close()


if __name__ == '__main__':
    limit = None
    if len(sys.argv) != 4:
        print("Usage: python MatriceGenarator M N P")
    else:
        try:
            m, n, p = [int(i) for i in sys.argv[1:4]]
            main(m, n, p)
        except ValueError:
            print(
                "Usage: python MatriceGenarator <M[:int]> <N[:int]> <P[:int]>")
