#include <vector>
#include <fstream>
#include <iostream>
#include "Core/Tensor.h"
#include "Core/Primitives.h"
#include "Threading/Task.h"
#include "Threading/ThreadPool.h"
#include "Threading/ThreadException.h"
#include <unistd.h>
using namespace std;
MutexLock *matWriteLock;
class Program
{
private:
	static vector<string> SplitString(string subject, string splitChars = " \t\n")
	{
		string buff = "";
		vector<string> ret;
		int sjLen = subject.length();
		int spLen = splitChars.length();
		for (int i = 0; i < sjLen; i++)
		{
			bool isSplit = false;
			for (int j = 0; j < spLen; j++)
				if (subject[i] == splitChars[j])
				{
					isSplit = true;
					break;
				}
			if (isSplit)
			{
				if (buff.length() > 0)
					ret.push_back(buff);
				buff.clear();
			}
			else
			{
				buff += subject[i];
			}
		}
		if (buff.length() > 0)
			ret.push_back(buff);
		return ret;
	}
	static void PrintMatrix(Tensor<float> *m, ostream &file)
	{
		auto size = m->GetSize();
		file << size[0] << '\t' << size[1];
		auto doPrint = [&file](ushort dims, llong *idx, float value) {
			if (idx[1] == 0)
				file << '\n';
			file << value << "\t";
		};
		m->ForEach(doPrint);
		file << endl;
	}
	static void *MatMultAction(ushort argc, void **argv)
	{
		ushort *argSize = (ushort *)(argv[0]);
		ushort w = argSize[0];
		ushort h = argSize[1];
		ushort k = argSize[2];
		Tensor<float> *matOne = (Tensor<float> *)(argv[1]);
		Tensor<float> *matTwo = (Tensor<float> *)(argv[2]);
		Tensor<float> *matRet = (Tensor<float> *)(argv[3]);
		llong *idx1 = new llong[2]{0, k};
		llong *idx2 = new llong[2]{k, 0};
		llong *idxR = new llong[2]{0, 0};
		float buffer;
		for (ushort i = 0; i < w; i++)
		{
			idx1[0] = i;
			idxR[0] = i;
			for (ushort j = 0; j < h; j++)
			{
				idxR[1] = j;
				idx2[1] = j;
				buffer = matOne->GetValueAt(2, idx1) * matTwo->GetValueAt(2, idx2);
				matWriteLock->Lock();
				buffer = buffer + matRet->GetValueAt(2, idxR);
				matRet->SetValueAt(2, idxR, buffer);
				matWriteLock->Unlock();
			}
		}
		return nullptr;
	}
	static Tensor<float> *MultMatParallel(Tensor<float> *mOne, Tensor<float> *mTwo, const ushort tCount)
	{
		llong *sizeOne = mOne->GetSize();
		llong *sizeTwo = mTwo->GetSize();
		if (sizeOne[1] != sizeTwo[0])
			throw BaseException("Invalid Size for Matrices. Cannot Multiply.");
		ushort w = sizeOne[0];
		ushort h = sizeTwo[1];
		ushort m = sizeOne[1];
		Tensor<float> *mRet = new Tensor<float>(2, new llong[2]{w, h}, [](ushort dc, llong *s) { return 0.0f; });
		// Divide Tasks by Rows
		matWriteLock = new MutexLock("lockForCout");
		Task **tasks = new Task *[m];
		ThreadPool pool("Pool", tCount);
		for (ushort c = 0; c < m; c++)
		{
			tasks[c] = new Task(Program::MatMultAction);
			// Task *t = new Task(Program::MultAction);
			tasks[c]->SetArgs(
				4,
				new void *[4] {
					(void *)(new ushort[3]{w, h, c}),
						(void *)mOne,
						(void *)mTwo,
						(void *)mRet
				});
		}
		pool.RunAll(m, tasks);
		pool.AwaitFreedomOfAll();
		return mRet;
	}
	static Tensor<float> *ParseMatrixFromFileStream(ifstream *fIn)
	{
		ushort w, h;
		string line = "";
		while (line.length() < 3)
			getline(*fIn, line, '\n');
		vector<string> lineParts = SplitString(line);
		w = stoi(lineParts[0]);
		h = stoi(lineParts[1]);
		float buffer = 0.0f;
		llong idx[2]{0, 0};
		Tensor<float> *ret = new Tensor<float>(2, new llong[2]{w, h});
		for (int x = 0; x < w; x++)
		{
			idx[0] = x;
			getline(*fIn, line, '\n');
			lineParts = Program::SplitString(line);
			if (lineParts.size() != h)
				throw BaseException("Invalid Matrix in File");
			for (int y = 0; y < h; y++)
			{
				idx[1] = y;
				float buff = stof(lineParts[y]);
				ret->SetValueAt(2, idx, buff);
			}
		}
		return ret;
	}
	static void Run(string fileIn, string fileOut, const ushort threadCount)
	{
		cout << "Matrix Mult : " << fileIn << " -> " << fileOut << ", using " << threadCount << " threads." << endl;
		cout << "Reading ..." << endl;
		ifstream fIn(fileIn);
		Tensor<float> *matOne = Program::ParseMatrixFromFileStream(&fIn);
		Tensor<float> *matTwo = Program::ParseMatrixFromFileStream(&fIn);
		fIn.close();
		cout << "Multiplying ..." << endl;
		Tensor<float> *matRes = Program::MultMatParallel(matOne, matTwo, threadCount);
		cout << "\nWriting ..." << endl;
		ofstream fOut(fileOut);
		Program::PrintMatrix(matRes, fOut);
		fOut.close();
		cout << "Finished ..." << endl;
	}

public:
	static void Main(Tensor<string> args)
	{
		ushort aCount = args.GetLength();
		ushort tCount = 4;
		string fileIn, fileOut = "Output.txt";
		// Parse Args
		switch (aCount)
		{
		case 4:
			fileOut = args.GetValueAt(3);
		case 3:
			tCount = atoi(args.GetValueAt(2).c_str());
		case 2:
			fileIn = args.GetValueAt(1);
			break;
		default:
			cout << "Invalid number of Arguments (" << (aCount - 1) << "), Expected 1, 2 or 3" << endl;
			cout << "Usage :\n\tMtMat.run <Input File> [Thread Count] [Output File]" << endl;
			return;
		}
		Program::Run(fileIn, fileOut, tCount);
	}
};
int main(int argc, char **argv)
{
	ushort len = (ushort)argc;
	Tensor<string> args(1, new llong[1]{len});
	for (ushort i = 0; i < len; i++)
		args.SetValueAt(i, string(argv[i]));
	try
	{
		Program::Main(args);
	}
	catch (ThreadException e)
	{
		cout << "Thread Exception (" << e.GetThread()->name << ") : " << e.GetMessage() << endl;
	}
	catch (RangeException e)
	{
		cout << "Range Exception : " << e.GetMessage() << endl;
		return -1;
	}
	catch (BaseException e)
	{
		cout << "Exception : " << e.GetMessage() << endl;
		return -1;
	}
	return 0;
}
