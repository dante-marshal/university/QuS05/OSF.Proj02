#ifndef __THREADING_LOCKS_MUTEX_LOCK_H__
#define __THREADING_LOCKS_MUTEX_LOCK_H__
#include <string>
#include <pthread.h>
struct MutexLock
{
private:
	std::string name;
	pthread_mutex_t *theLock;

public:
	MutexLock(std::string name);
	~MutexLock();
	void Lock();
	int TryLock();
	void LockTimed(const timespec *time);
	void Unlock();
	std::string GetName();
	pthread_mutex_t *GetValue();
};
#endif