#ifndef __THREADING_LOCKS_CONDITIONAL_LOCK_H__
#define __THREADING_LOCKS_CONDITIONAL_LOCK_H__
#include <pthread.h>
#include "MutexLock.h"
struct ConditionVar
{
public:
	ConditionVar(const char *name);
	~ConditionVar();
	void Await(MutexLock *theMutexToRelease);
	void Signal();
	void Broadcast();

private:
	const char* name;
	pthread_cond_t *theConditional;
};
#endif