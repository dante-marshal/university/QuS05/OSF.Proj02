#include <iostream>
#include "MutexLock.h"
using namespace std;
MutexLock::MutexLock(string name)
{
	this->name = name;
	this->theLock = new pthread_mutex_t();
	pthread_mutex_init(this->theLock, NULL);
}
MutexLock::~MutexLock()
{
	pthread_mutex_unlock(this->theLock);
	pthread_mutex_destroy(this->theLock);
}
void MutexLock::Lock()
{
	// cout << (pthread_self() % 100) << " : Locking (" << this->name << ") : Blocking" << endl;
	pthread_mutex_lock(this->theLock);
	// cout << (pthread_self() % 100) << " : Locked (" << this->name << ")" << endl;
}
int MutexLock::TryLock()
{
	// cout << (pthread_self() % 100) << " : Locking (" << this->name << ") : Trying" << endl;
	return pthread_mutex_trylock(this->theLock);
}
void MutexLock::LockTimed(const timespec *time)
{
	// cout << (pthread_self() % 100) << " : Locking (" << this->name << ") : Timed (" << time << ")" << endl;
	pthread_mutex_timedlock(this->theLock, time);
}
void MutexLock::Unlock()
{
	// cout << (pthread_self() % 100) << " : Unlocking (" << this->name << ")" << endl;
	pthread_mutex_unlock(this->theLock);
}
string MutexLock::GetName()
{
	return this->name;
}
pthread_mutex_t *MutexLock::GetValue()
{
	return this->theLock;
}
