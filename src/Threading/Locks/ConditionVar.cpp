#include <stdio.h>
#include <string.h>
#include <iostream>
#include <pthread.h>
#include "ConditionVar.h"
using namespace std;
class LockWaitException : std::exception
{
public:
	const char *what()
	{
		return "A Wait on a Condition Variable has Failed !";
	}
};
ConditionVar::ConditionVar(const char *name)
{
	this->name = name;
	// name + ":TheMutex"
	char *mutexName = new char[strlen(name) + 10];
	strcat(mutexName, ":TheMutex");
	this->theConditional = new pthread_cond_t();
	pthread_cond_init(this->theConditional, NULL);
}
ConditionVar::~ConditionVar()
{
	pthread_cond_destroy(this->theConditional);
}
void ConditionVar::Await(MutexLock *theMutexToRelease)
{
	string mxName = theMutexToRelease->GetName();
	pthread_mutex_t *mutex = theMutexToRelease->GetValue();
	// cout << (pthread_self() % 100) << " : Waiting to Unlock (" << this->name << " @ " << mxName << ")" << endl;
	if (int err = pthread_cond_wait(this->theConditional, mutex))
		throw LockWaitException();
	// cout << (pthread_self() % 100) << " : Unlocked (" << this->name << " @ " << mxName << ")" << endl;
}
void ConditionVar::Signal()
{
	// cout << (pthread_self() % 100) << " : Signaling (" << this->name << ")" << endl;
	pthread_cond_signal(this->theConditional);
}
void ConditionVar::Broadcast()
{
	// cout << (pthread_self() % 100) << " : Broadcasting (" << this->name << ")" << endl;
	pthread_cond_broadcast(this->theConditional);
}
