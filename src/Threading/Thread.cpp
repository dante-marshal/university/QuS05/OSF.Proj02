#include <iostream>
#include "Thread.h"
#include "ThreadException.h"
using namespace std;
// Thread Class
Thread::Thread(string name, bool detachOnDestroy)
{
	// Set Fields
	this->name = name;
	this->mxWrite = new MutexLock("Thread mxWrite");
	this->mxTask = new MutexLock("Thread mxTask");
	this->cvRun = new ConditionVar("Thread cvRun");
	this->targetTask = nullptr;
	this->status = ThreadStatus::SttCreated;
	this->willDetachOnDestroy = detachOnDestroy;
	// Lock before starting the thread
	this->mxTask->Lock();
	// Create the Thread and Start the Main Loop...
	if (pthread_create(&(this->tId), NULL, &(Thread::InvokeMainLoop), (void *)this))
		throw ThreadException(this, ThreadStatus::ErrCreationFailed);
}
Thread::~Thread()
{
	delete this->cvRun;
	delete this->mxTask;
}
void Thread::MainLoop()
{
	// cout << endl << pthread_self() << " : Starting Main loop on " << this->name << endl;
	while (true)
	{
		this->status = ThreadStatus::SttWaiting;
		// Wait for a new Request
		this->cvRun->Await(this->mxTask);
		// cout << (this->GetThreadId() % 100) << " : Has received a Signal !" << endl;
		// Check for if the Task is empty, we need to break
		if (this->targetTask == nullptr)
		{
			// It's a Termination signal
			// cout << (this->GetThreadId() % 100) << " : Terminating ..." << endl;
			this->status = ThreadStatus::SttTerminated;
			return;
		}
		// Otherwise, Run the Task
		try
		{
			// cout << (this->tId % 100) << " : Running the Task ..." << endl;
			// Yes, Run it ...
			this->targetTask->Run();
		}
		catch (...)
		{
			cerr << "Unknown Exception Inside Thread !" << endl;
			this->status = ThreadStatus::ErrRunFailed;
			throw;
		}
		// Now that the Task is Complete, Clear it
		this->targetTask = nullptr;
	}
}
// Thread Controls
void Thread::AwaitFreedom()
{
	while (this->status == ThreadStatus::SttRunning)
	{
		this->mxTask->Lock();
		this->mxTask->Unlock();
	}
}
void Thread::Run(Task *task)
{
	if (task == nullptr)
		// Nothing to Run
		return;
	// Aquire the Write Lock
	this->mxWrite->Lock();
	// Wait for previous task to complete
	this->mxTask->Lock();
	// Make sure we're not still running
	if (this->status == ThreadStatus::SttRunning)
		throw ThreadException(this, ThreadStatus::ErrAlreadyBusy);
	this->status = ThreadStatus::SttRunning;
	// Set the Task
	this->targetTask = task;
	// Signal the thread to Continue
	this->mxTask->Unlock();
	this->cvRun->Signal();
	// Release the Write lock
	this->mxWrite->Unlock();
}
void Thread::Run(TaskAction action, ushort argc, void **argv)
{
	Task *task = new Task(action);
	task->SetArgs(argc, argv);
	this->Run(task);
}
bool Thread::TryRun(Task *task)
{
	try
	{
		Run(task);
		return true;
	}
	catch (ThreadException)
	{
		return false;
	}
}
bool Thread::TryRun(TaskAction action, ushort argc, void **argv)
{
	try
	{
		Run(action, argc, argv);
		return true;
	}
	catch (ThreadException)
	{
		return false;
	}
}
// Thread Behaviors
void Thread::Join()
{
	// cout << endl << pthread_self() << " : Joining Thread " << this->name << endl;
	if (pthread_join(this->tId, NULL))
		throw ThreadException(this, ThreadStatus::ErrJoinFailed);
}
void Thread::Detach()
{
	// cout << endl << pthread_self() << " : Detaching Thread " << this->name << endl;
	if (pthread_detach(this->tId))
		throw ThreadException(this, ThreadStatus::ErrDetachFailed);
}
void Thread::Terminate()
{
	// Aquire the Lock
	this->mxTask->Lock();
	// Set the Task to Null, So the Main loop Terminates
	this->targetTask = nullptr;
	// Release the Lock, So the thread can continue afterwards
	this->mxTask->Unlock();
	// Signal the thread to Continue
	this->cvRun->Signal();
}
// Static Helpers
void *Thread::InvokeMainLoop(void *self)
{
	Thread *thread = ((Thread *)self);
	try
	{
		thread->MainLoop();
	}
	catch (ThreadStatus tErr)
	{
		thread->status = tErr;
	}
	return self;
}
