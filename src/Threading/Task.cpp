#include <iostream>
#include "Task.h"
using namespace std;

Task::Task(TaskAction action)
{
	this->action = action;
}
void Task::Run()
{
	// cout << "Requesting to Run a Task with " << this->argc << " args" << endl;
	this->action(this->argc, this->argv);
}
void Task::SetArgs(ushort argc, void **argv)
{
	this->argc = argc;
	this->argv = argv;
}
void *Task::GetResult()
{
	return this->result;
}