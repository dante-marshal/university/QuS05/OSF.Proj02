#ifndef __THREADING_THREAD_H__
#define __THREADING_THREAD_H__
#include <string>
#include <pthread.h>
#include "ThreadStatus.h"
#include "../Core/Primitives.h"
#include "Locks/ConditionVar.h"
#include "../Core/Errors/BaseException.h"
#include "Task.h"
class Thread
{
public:
	// Thread Statistics
	std::string name;
	pthread_t GetThreadId() { return this->tId; }
	ThreadStatus GetStatus() { return this->status; }
	// Thread Controls
	void AwaitFreedom();
	void Run(Task *task);
	void Run(TaskAction action, ushort argc, void **argv);
	bool TryRun(Task *task);
	bool TryRun(TaskAction action, ushort argc, void **argv);
	// Thread Behavior
	void Join();
	void Detach();
	void Terminate();
	// Class Stuff
	Thread(string name, bool detachOnDestroy = false);
	~Thread();

private:
	// Thread Stuff
	pthread_t tId;
	ThreadStatus status;
	bool willDetachOnDestroy;
	// Thread Behaviors
	void MainLoop();
	Task *targetTask;
	// Locks
	ConditionVar *cvRun;
	MutexLock *mxTask, *mxWrite;
	// Thread Worker
	static void *InvokeMainLoop(void *self);
};
#endif
