#ifndef __THREADING_THREAD_STATUS_H__
#define __THREADING_THREAD_STATUS_H__
#include <string>
enum ThreadStatus
{
	Unknown,
	// Errors
	ErrRunFailed,
	ErrCreationFailed,
	ErrJoinFailed,
	ErrDetachFailed,
	ErrTaskNotFound,
	ErrAlreadyBusy,
	// Status
	SttCreated,
	SttWaiting,
	SttRunning,
	SttTerminated
};
// Helpers
std::string GetThreadStatusName(ThreadStatus status);
#endif