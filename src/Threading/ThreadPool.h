#ifndef __THREADING_THREAD_POOL_H__
#define __THREADING_THREAD_POOL_H__
#include "../Core/Tensor.h"
#include "Thread.h"
class ThreadPool
{
private:
	Thread ** threads;
	ushort threadCount = 0;

public:
	ThreadPool(string name, ushort threadCount, bool detachOnDestroy = false);
	void Run(Task *task);
	void Run(TaskAction action, ushort argc, void **argv);
	bool TryRun(Task *task);
	bool TryRun(TaskAction action, ushort argc, void **argv);
	void RunAll(ushort count, Task **tasks);
	void AwaitFreedomOfAll();
};
#endif