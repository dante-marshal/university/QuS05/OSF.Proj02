#include <string>
#include "ThreadPool.h"
#include "ThreadException.h"
using namespace std;
ThreadPool::ThreadPool(string name, ushort threadCount, bool detachOnDestroy)
{
	this->threadCount = threadCount;
	this->threads = new Thread *[this->threadCount];
	for (ushort i = 0; i < threadCount; i++)
		this->threads[i] = new Thread(name + "(" + to_string(i) + ")", detachOnDestroy);
}
void ThreadPool::Run(Task *task)
{
	for (ushort i = 0; i < this->threadCount; i++)
	{
		if (this->threads[i]->GetStatus() == ThreadStatus::SttRunning)
			// It's busy, Get the next one !
			continue;
		if (this->threads[i]->TryRun(task))
			// Task is Running, Return now
			return;
	}
	// Failed to find a free thread
	throw ThreadException(nullptr, ThreadStatus::ErrAlreadyBusy);
}
void ThreadPool::Run(TaskAction action, ushort argc, void **argv)
{
	Thread *t;
	for (ushort i = 0; i < this->threadCount; i++)
	{
		t = this->threads[i];
		if (t->TryRun(action, argc, argv))
			return;
	}
	throw ThreadException(nullptr, ThreadStatus::ErrAlreadyBusy);
}
bool ThreadPool::TryRun(Task *task)
{
	try
	{
		this->Run(task);
		return true;
	}
	catch (ThreadException)
	{
		return false;
	}
}
bool ThreadPool::TryRun(TaskAction action, ushort argc, void **argv)
{
	try
	{
		this->Run(action, argc, argv);
		return true;
	}
	catch (ThreadException te)
	{
		cout << te.GetThread()->name << " : " << te.GetMessage() << endl;
		return false;
	}
}
void ThreadPool::RunAll(ushort count, Task **tasks)
{
	for (ushort i = 0; i < count; i++)
	{
		while (!(this->TryRun(tasks[i])))
		{
			// Loop until found a thread is free to run
		}
	}
	// All tasks are running, some of them finished maybe
}
void ThreadPool::AwaitFreedomOfAll()
{
	// Wait until all threads are free
	for (ushort i = 0; i < this->threadCount; i++)
		this->threads[i]->AwaitFreedom();
}
