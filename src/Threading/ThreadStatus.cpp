#include "ThreadStatus.h"
std::string GetThreadStatusName(ThreadStatus status)
{
	switch (status)
	{
	case ThreadStatus::ErrRunFailed:
		return "Error (Run Failed)";
	case ThreadStatus::ErrCreationFailed:
		return "Error (Creation Failed)";
	case ThreadStatus::ErrJoinFailed:
		return "Error (Join Failed)";
	case ThreadStatus::ErrDetachFailed:
		return "Error (Detach Failed)";
	case ThreadStatus::ErrTaskNotFound:
		return "Error (Task not Found)";
	case ThreadStatus::ErrAlreadyBusy:
		return "Error (Already Busy)";
	case ThreadStatus::SttCreated:
		return "Created";
	case ThreadStatus::SttWaiting:
		return "Waiting";
	case ThreadStatus::SttRunning:
		return "Running";
	case ThreadStatus::SttTerminated:
		return "Terminated";
	default:
		return "Unknown";
	}
}
