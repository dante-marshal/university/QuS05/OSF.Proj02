#ifndef __THREADING_TASK_H__
#define __THREADING_TASK_H__
#include "../Core/Primitives.h"
typedef void* (*TaskAction)(ushort, void **);
struct Task
{
private:
	ushort argc = 0;
	void **argv = nullptr;
	void *result = nullptr;

public:
	Task(TaskAction action);
	void Run();
	void *GetResult();
	void SetArgs(ushort argc, void **argv);

private:
	TaskAction action;
};
#endif