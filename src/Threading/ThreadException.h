#ifndef __THREADING_THREAD_EXCEPTION__
#define __THREADING_THREAD_EXCEPTION__
#include "Thread.h"
struct ThreadException : BaseException
{
private:
	Thread *thread;
	ThreadStatus status;

public:
	Thread *GetThread() { return this->thread; }
	ThreadStatus GetStatus() { return this->status; }
	ThreadException(Thread *thread, ThreadStatus status) : BaseException(GetThreadStatusName(status))
	{
		this->thread = thread;
		this->status = status;
	}
	ThreadException(Thread *thread, ThreadStatus status, string message) : BaseException(message)
	{
		this->thread = thread;
		this->status = status;
	}
};
#endif