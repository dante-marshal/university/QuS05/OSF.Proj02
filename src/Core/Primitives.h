#ifndef __CORE_PRIMITIVES_H__
#define __CORE_PRIMITIVES_H__
#include <string>
#include <functional>
typedef long long llong;
typedef std::string string;
typedef unsigned short ushort;
typedef unsigned long int ulong;
typedef long unsigned long lulong;
template <typename TReturn, typename... TArgs>
using func = std::function<TReturn(TArgs...)>;
#endif