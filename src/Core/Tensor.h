#ifndef __CORE_ARRAY_H__
#define __CORE_ARRAY_H__
#include <iostream>
#include <functional>
#include "Primitives.h"
#include "Errors/RangeException.h"
template <typename T>
using TensorInitializer = T (*)(ushort, llong *);
template <typename T>
using TensorIterator = std::function<void(ushort, llong*, T)>; // [](ushort, llong *, T) TensorIterator -> void;
template <typename T>
struct Tensor
{
private:
	T *values;
	llong *size;
	llong maxSize;
	ushort dimCount;

public:
	llong GetFlatIndexAt(llong *index)
	{
		// 1D : d[0]
		// 2D : d[0] + s[0] * d[1]
		// 3D : d[0] + s[0] * d[1] + s[0] * s[1] * d[2]
		// ...
		// iD : d[0] + s[0] * d[1] + ... + Mult(s, 0 to i) * d[i]
		llong ret = 0;
		for (ushort i = 0; i < this->dimCount; i++)
		{
			llong idx = index[i];
			for (ushort j = 0; j < i; j++)
				idx *= this->size[j];
			ret += idx;
		}
		return ret;
	}
	static void ThrowDimensionError(llong expected, llong received)
	{
		RangeException x("Invalid Dimensionality; Expected ");
		x.ConcatToMessage(expected);
		x.ConcatToMessage(", Received : ");
		x.ConcatToMessage(received);
		throw x;
	}
	static void ThrowOutOfRange(llong index, llong max)
	{
		RangeException x("Index '");
		x.ConcatToMessage(index);
		x.ConcatToMessage("' Out of Range (0, ");
		x.ConcatToMessage(max);
		x.ConcatToMessage(")");
		throw x;
	}

public:
	Tensor(ushort dimCount, llong *size, TensorInitializer<T> initializer = nullptr)
	{
		this->size = size;
		this->dimCount = dimCount;
		this->maxSize = 1;
		for (ushort i = 0; i < this->dimCount; i++)
			this->maxSize *= this->size[i];
		this->values = new T[this->maxSize];
		if (initializer != nullptr)
			this->FillValuesUsing(initializer);
	}
	void FillValuesUsing(TensorInitializer<T> initializer)
	{
		llong *index = new llong[this->dimCount];
		for (ushort i = 0; i < this->dimCount; i++)
			index[i] = 0;
		bool shallContinue = this->dimCount > 0;
		while (shallContinue)
		{
			this->SetValueAt(this->GetFlatIndexAt(index), initializer(this->dimCount, index));
			// Increment Index
			shallContinue = false;
			for (int i = this->dimCount - 1; i >= 0; i--)
			{
				if (index[i] < (this->size[i] - 1))
				{
					index[i] = index[i] + 1;
					shallContinue = true;
					break;
				}
				else
				{
					index[i] = 0;
				}
			}
		};
	}
	void ForEach(TensorIterator<T> doStuff)
	{
		llong *index = new llong[this->dimCount];
		for (ushort i = 0; i < this->dimCount; i++)
			index[i] = 0;
		bool shallContinue = this->dimCount > 0;
		while (shallContinue)
		{
			doStuff(this->dimCount, index, this->GetValueAt(this->dimCount, index));
			// Increment Index
			shallContinue = false;
			for (int i = this->dimCount - 1; i >= 0; i--)
			{
				if (index[i] < (this->size[i] - 1))
				{
					index[i] = index[i] + 1;
					shallContinue = true;
					break;
				}
				else
				{
					index[i] = 0;
				}
			}
		};
	}
	// Getters
	T GetValueAt(llong index)
	{
		if (index < 0 || index >= maxSize)
			Tensor::ThrowOutOfRange(index, maxSize - 1);
		return values[index];
	}
	T GetValueAt(llong x, llong y) // for 2D
	{
		return this->GetValueAt(2, new llong[2]{x, y});
	}
	T GetValueAt(llong x, llong y, llong z) // for 3D
	{
		return this->GetValueAt(3, new llong[3]{x, y, z});
	}
	T GetValueAt(ushort dimCount, llong *indices)
	{
		if (dimCount != this->dimCount)
			Tensor::ThrowDimensionError(this->dimCount, dimCount);
		return this->GetValueAt(this->GetFlatIndexAt(indices));
	}
	// Setters
	void SetValueAt(llong index, T value)
	{
		if (index > maxSize || index < 0)
			Tensor::ThrowOutOfRange(index, maxSize - 1);
		this->values[index] = value;
	}
	void SetValueAt(ushort dimCount, llong *index, T value)
	{
		if (dimCount != this->dimCount)
			Tensor::ThrowDimensionError(this->dimCount, dimCount);
		this->SetValueAt(this->GetFlatIndexAt(index), value);
	}
	// Info
	llong *GetSize()
	{
		llong *ret = new llong[this->dimCount];
		for (ushort i = 0; i < this->dimCount; i++)
			ret[i] = this->size[i];
		return ret;
	}
	llong *GetSize(ushort &dimCount)
	{
		dimCount = this->dimCount;
		return this->GetSize();
	}
	llong GetLength() { return this->maxSize; }
	ushort GetDimensionality() { return this->dimCount; }
};
#endif