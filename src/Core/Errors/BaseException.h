#ifndef _CORE_ERRORS_BASE_EXCEPTION_H_
#define _CORE_ERRORS_BASE_EXCEPTION_H_
#include <sstream>
#include <exception>
#include "../Primitives.h"
struct BaseException
{
protected:
	string message;

public:
	virtual string GetMessage() { return this->message; }
	BaseException(string message) { this->message = message; }
	// Concat
	template <typename TMsg>
	void ConcatToMessage(TMsg msg)
	{
		std::ostringstream oss;
		oss << this->message << msg;
		this->message = oss.str();
	}
};
#endif