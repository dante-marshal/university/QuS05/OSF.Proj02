#ifndef _CORE_ERRORS_RANGE_EXCEPTION_H_
#define _CORE_ERRORS_RANGE_EXCEPTION_H_
#include "BaseException.h"

struct RangeException : BaseException
{
public:
	RangeException(string message) : BaseException(message) {}
};
#endif